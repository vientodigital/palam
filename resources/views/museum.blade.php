@extends('layouts.app')

@section('header')
<header class="h-100 bg-cover bg-no-repeat bg-top" style="background-image: url({{url('storage/images/slidePalacioMusica-02.jpg')}})">
</header>
@endsection

@section('content')
<div class="flex flex-col lg:flex-row container mx-auto ">
    <section class="px-6 lg:w-1/2">
        <h3 class="text-2xl text-palam-500 font-bold uppercase pt-6">Museo</h3>
        <p class="text-gray-700 antialiased">
            El museo ofrece un recorrido por la historia de la música tradicional y popular
            mexicana. Presenta una revisión de géneros, instrumentos, compositores e intérpretes;
            así como de la música en distintas regiones del país y diferentes épocas de su historia,
            los medios de difusión y las influencias de otras culturas, países o regiones, entre otros
            aspectos, a través de audios, videos, multimedios interactivos, hologramas y objetos de
            colección, entre otros recursos, que favorecen el acercamiento al patrimonio musical.
        </p>

        <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6"> Planifica tu visita </h4>
        <p class="text-gray-700 antialiased">

            El área de exposición tiene una gran cantidad de información que se presenta en el sistema cedulario,
            ambientes y gráficos. En este sentido, puede ser importante que conozcas un poco de los contenidos y
            experiencias del museo, para que programes la visita más adecuada a tu tiempo y tus intereses.

        </p>

        <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6"> Horario</h4>
        <p class="text-gray-700 antialiased">
            Museo abierto de martes a domingo de 10:00 a.m. a 4:00 p.m.
        </p>


        <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6">Tarifas </h4>
        <ul class="text-gray-700 antialiased list-none mt-4">
            <li>Entrada General: $150.00 MXN</li>
            <li>Nacionales: $100.00 MXN</li>
            <li>Residentes de Yucatán: $50.00 MXN</li>
            <li>Menores 0-3 años: $0.00 MXN</li>
            <li>Menores 4-12 años: $25.00 MXN</li>
            <li>Menores 13-17 años: $50.00 MXN</li>
            <li>Maestros y estudiantes: $25.00 MXN</li>
            <li>Adultos mayores (INAPAM): $25.00 MXN</li>
            <li>Personas con discapacidad: $25.00 MXN</li>

        </ul>

        <span class="inline-block font-semibold uppercase mt-4">Martes</span>
        <p class="text-gray-700 antialiased">
            Entrada libre para residentes de Yucatán al presentar físicamente documento oficial vigente (INE, licencia de conducir o pasaporte). Los menores de edad que acompañen a los acreditados entrarán gratis.
        </p>

        <section class="container mx-auto mt-4">
            <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6">Secciones temáticas:</h4>
            <div class="mt-4">
                <div class="flex items-start">
                    <div>
                        <div class="h-8 w-8 " style="background-color:#A46C38;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold"> Bienvenida e introducción</span>
                        <p class="text-gray-700 antialiased"> Realizarás una iniciación acerca de los procesos de conformación de múltiples expresiones musicales. </p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>
                        <div class="h-8 w-8" style="background-color:#755F16;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">Antecedentes (Sonidos prehispánicos, música colonial y música del siglo XIX)</span>
                        <p class="text-gray-700 antialiased">Recorrerás desde la herencia del pasado prehispánico y el encuentro cultural del período colonial, hasta los primeros intentos de construcción nacional del México independiente del siglo XIX. </p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>
                        <div class="h-8 w-8" style="background-color:#452775;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">Culturas musicales</span>
                        <p class="text-gray-700 antialiased">Disfrutarás de las múltiples culturas musicales tradicionales que conforman el patrimonio sonoro de México. Expresiones colectivas, transmitidas de generación en generación, que mantienen una relación difícil de delimitar entre la continuidad de las tradiciones puras y las transformaciones derivadas de los nuevos tiempos.</p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>

                        <div class="h-8 w-8" style="background-color:#F8B133;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">Música yucateca (Jarana y canción)</span>
                        <p class="text-gray-700 antialiased">Conocerás las expresiones musicales más características de Yucatán. La jarana, la cual se baila en las vaquerías durante las fiestas patronales de las diferentes localidades del estado y la canción o trova yucateca que, influenciada por la música cubana, ha sido una aportación fundamental para la música popular mexicana.</p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>

                        <div class="h-8 w-8" style="background-color:#3A5888;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">La música en los medios (Teatro, radio, disco, cine, televisión)</span>
                        <p class="text-gray-700 antialiased">Hallarás los medios que fueron fundamentales para la difusión de la música a través de todo el país: el teatro, la radio, el disco, el cine y la televisión.</p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>

                        <div class="h-8 w-8" style="background-color:#5BB587;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">Protagonistas de la música (Autores y compositores, instrumentistas e intérpretes)</span>
                        <p class="text-gray-700 antialiased">Admirarás los compositores, autores, instrumentistas e intérpretes que han dado forma a la música popular mexicana. Aquellos que lograron obtener con su labor creativa y su habilidad expresiva, piezas musicales inolvidables.</p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>

                        <div class="h-8 w-8" style="background-color:#B11537;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">Conformación y expansión musical</span>
                        <p class="text-gray-700 antialiased">Encontrarás la conexión creativa entre el pasado y el presente, los géneros y estilos más antiguos de la música mexicana.</p>
                    </div>
                </div>
                <div class="flex items-start mt-3">
                    <div>

                        <div class="h-8 w-8" style="background-color:#B11537;"></div>
                    </div>
                    <div class="ml-4">
                        <span class="font-semibold">Reflexión final</span>
                        <p class="text-gray-700 antialiased">Descubrirás que la música de México es indicio, memoria y testigo presente de un país pluricultural, en el cual coexisten un sinfín de gustos, expresiones, interrelaciones y necesidades humanas; sin importar edad, género, contexto geográfico ni condición social.</p>
                    </div>
                </div>
            </div>
            <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6">mapa de zonificación </h4>
            <p class="text-gray-700 antialiased mb-6">También puedes acercarte al equipo de guías del museo o, si tú lo
                prefieres, puedes dejarte
                llevar por tu
                gusto o intuición.</p>

        </section>
        <div class="mt-4">
            <img class="" src="{{asset('storage/images/Zonificacion-Palacio-Musica.png')}}" alt="">
        </div>
        <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6"> Recorridos guiados </h4>
        <p class="text-gray-700 antialiased">

            Si vas a visitar el área de exhibición permanente en grupo, el Palacio de la Música ofrece recorridos guiados adecuados a sus necesidades específicas.

        </p>
        <h4 class="text-2xl text-palam-500 font-bold uppercase pt-6"> RECORRIDOS ESCOLARES GRATUITOS </h4>
        <p class="text-gray-700 antialiased mb-8">
            Los grupos educativos, integrados por estudiantes escolares o universitarios y sus profesores.
        </p>
        <div class="mt-4 mb-8">
            <a class="inline-block btn btn-primary" href="{{url('/tour')}}">Solicita recorrido</a>
        </div>


    </section>
    <div class="lg:w-1/2 flex flex-col">
        <div class="flex lg:flex-col items-end">
            <div class="lg:pl-8 w-full">
                <img class="mt-4 block w-full" src="{{asset('storage/images/IGH-72.png')}}" alt="">
            </div>
            <div class="lg:pl-8 w-full">
                <img class="mt-4  block w-full" src="{{asset('storage/images/IGH-96.png')}}" alt="">
            </div>
        </div>
        <div class="mt-8">
            <div class="lg:pl-8">
                <h3 class="py-4 px-6 text-gray-100 font-bold text-center bg-palam-500 uppercase">Actividades y Talleres</h3>
                @forelse($space->events as $evt)
                <div class="bg-gray-300 py-4 @if($loop->index !== 0) hidden @endif">
                    <div class="flex justify-between items-center text-gray-500">
                        <div class="text-3xl px-8 font-black">&lt; </div>
                        <div><img class="w-full object-cover" src='{{$evt->photo_url}}' alt='' /></div>
                        <div class="text-3xl px-8 font-black">&gt; </div>
                    </div>
                    <div class="mt-4 px-8">
                        <h2 class="font-semibold">{{$evt->title}}</h2>
                        <div> {{$evt->description}} </div>
                        <p class="mt-4">
                            {!!$evt->content!!}
                        </p>
                    </div>

                </div>
                @empty
                No hay eventos en Museo Interactivo
                @endforelse
            </div>
        </div>
        <div class="flex lg:flex-col items-end">
            <div class="lg:pl-8 w-full">
                <img class="mt-4 block w-full" src="{{asset('storage/images/01_museo_cine.jpg')}}" alt="">
            </div>
            <div class="lg:pl-8 w-full">
                <img class="mt-4  block w-full" src="{{asset('storage/images/02_museo_trivia.jpg')}}" alt="">
            </div>
            <div class="lg:pl-8 w-full">
                <img class="mt-4  block w-full" src="{{asset('storage/images/03_museo_instrumentos.jpg')}}" alt="">
            </div>
            <div class="lg:pl-8 w-full">
                <img class="mt-4  block w-full" src="{{asset('storage/images/04_museo_tocadisco.jpg')}}" alt="">
            </div>
            <div class="lg:pl-8 w-full">
                <img class="mt-4 lg:mb-4  block w-full" src="{{asset('storage/images/05_museo_mesa.jpg')}}" alt="">
            </div>
        </div>


    </div>

</div>
@endsection