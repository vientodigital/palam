<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{asset('storage/images/favicon.png')}}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Palacio de la música') }}</title>
    <meta property="og:url" content="{{url('')}}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{setting('site-title',config('app.name', 'Palacio de la música'))}}" />
    <meta property="og:description" content="{{setting('site-description','Una experiencia auditiva, visual e interactiva única sobre la evolución de la canción popular de México y Yucatán.')}}'" />
    <meta property="og:image" content="{{asset('storage/images/slidePalacioMusica-03.jpg')}}" />
    <meta name="description" content="{{setting('site-description','Una experiencia auditiva, visual e interactiva única sobre la evolución de la canción popular de México y Yucatán.')}}'">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('head')
</head>

<body class="min-h-screen bg-palam-500">
    <div id="sidenav" class="fixed bg-palam-700 min-h-screen z-10">
        <a id="close" href="javascript:void(0)" class="block pr-4 text-right text-3xl text-palam-200 hover:text-palam-300" onclick="toggleNav()">&times;</a>

        <a class="menu-item ml-4" href="{{route('home')}}">Inicio</a>
        <a class="menu-item ml-4">Palacio</a>
        <a class="menu-item ml-8" href="{{route('museum')}}">Museo Interactivo</a>
        <a class="menu-item ml-8" href="{{route('spaces')}}">Espacios y servicios</a>
        <a class="menu-item ml-8" href="{{route('sound-library')}}">Fonoteca</a>
        <a class="menu-item ml-4" href="{{route('events')}}">Eventos</a>
        <a class="menu-item ml-4" href="{{route('post',['section'=>'prensa'])}}">Prensa</a>
        <a class="menu-item ml-4">Transparencia </a>
        <a class="menu-item ml-8" href="https://www.plataformadetransparencia.org.mx/web/guest/inicio">
            Plataforma nacional de Transparencia </a>
        <a class="menu-item ml-8" href="https://infomex.transparenciayucatan.org.mx/InfomexYucatan/">Plataforma estatal de Transparencia </a>
        <a class="menu-item ml-4" href="{{route('accounting')}}">Contabilidad gubernamental</a>
        <a class="menu-item ml-4" href="{{route('contact')}}">Contacto</a>
    </div>
    <div id="app" class="overflow-x-hidden">
        <div class="fixed z-50 min-w-screen bg-palam-nav">
            <nav class="mx-4 my-4 flex items-center justify-between md:justify-start">
                <a class="text-white" href="javascript:toggleNav()">
                    <svg class="fill-current w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                        <path d="M4 10h24a2 2 0 0 0 0-4H4a2 2 0 0 0 0 4zm24 4H4a2 2 0 0 0 0 4h24a2 2 0 0 0 0-4zm0 8H4a2 2 0 0 0 0 4h24a2 2 0 0 0 0-4z" />
                    </svg>
                </a>
                <a href="{{ url('/') }}" class="text-white md:ml-8" title="Palacio de la Musica">
                    @svg('palam', 'text-white h-16 md:h-24', [])
                </a>
            </nav>
        </div>
        <div>
            @yield('header')
            <main class="bg-white">
                @yield('content')
            </main>
        </div>
        <footer class="bg-palam-700">
            <div class="hidden bg-palam-800">
                <div class="container mx-auto text-center text-sm uppercase leading-relaxed py-4 flex flex-col md:flex-row justify-around items-center">
                    <a class="text-white" target="_blank" href="https://www.plataformadetransparencia.org.mx/web/guest/inicio">
                        Plataforma nacional de transparencia
                    </a>
                    <a class="text-white" href="{{route('accounting')}}">
                        Contabilidad gubernamental
                    </a>
                    <a class="text-white" target="_blank" href="https://infomex.transparenciayucatan.org.mx/InfomexYucatan/">
                        Plataforma estatal de transparencia
                    </a>
                </div>
            </div>
            <div class="container mx-auto mb-8 text-white">
                <div class="flex items-center justify-between ml-4 md:ml-0 pb-6">
                    @svg('yucatan', 'text-white h-10', [])
                    <div>
                        <div class="flex flex-col lg:flex-row items-center justify-around md:justify-end mt-6">
                            <div class="lg:ml-4 mt-2">
                                <a class="inline-block h-8" href="{{setting('facebook','https://www.facebook.com/PalacioDeLaMusicaCNMM')}}" target="_blank">
                                    @svg('facebook','h-8',[])
                                </a>
                            </div>
                            <div class="lg:ml-4 mt-2">
                                <a class="inline-block w-8 h-8" href="{{setting('instagram','https://www.instagram.com/palaciodelamusicacnmm')}}" target="_blank">
                                    @svg('instagram','h-8')
                                </a>
                            </div>
                            <div class="lg:ml-4 mt-2">
                                <a class="inline-block w-8 h-8" href="https://spoti.fi/36KQY1t" target="_blank">
                                    @svg('spotify','h-8')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="bg-gray-100 opacity-50">
                <div class="flex justify-between">

                    <div class="text-white text-xs mt-4">
                        <h5 class="text-sm font-semibold">Palacio de la Música</h5>
                        <address class="not-italic whitespace-pre-line">
                            <span class="font-semibold">Dirección:</span> {!!setting('direccion','Calles 59 No. 497, <br>Centro Histórico, <br>Mérida, Yucatán.',true)!!}
                            <span class="font-semibold">Teléfono:</span> {{setting('telefono','923 0641')}}
                        </address>
                    </div>
                    <div class="text-white mt-4">
                        <a class="text-center" href="{{url('storage/documents/PALAM-AVISO-DE-PRIVACIDAD.pdf')}}" target="_blank" download="aviso-de-privacidad">Aviso de privacidad</a>
                    </div>

                </div>

            </div>
            <div class="bg-palam-800 py-6 text-white text-center uppercase">
                Gobierno del Estado de Yucatán 2018-2024
            </div>

        </footer>
    </div>
    @yield('tail')
</body>

</html>