@extends('layouts.app')

@section('header')
<header class="h-100 bg-cover bg-no-repeat bg-bottom" style="background-image: url({{url('storage/images/slidePalacioMusica.jpg')}})">
</header>
@endsection

@section('content')
<section class="container mx-auto">
    <div class="flex flex-col items-start justify-between lg:flex-row px-6 pt-8">
        <div class="flex-1">
            <h1 class="text-palam-500 text-2xl uppercase">Recorridos Escolares Gratuitos</h1>

            <p class="text-gray-800 mt-4 whitespace-pre-line">
                <h4>Horarios.</h4>
                <div class="whitespace-pre-line">{{setting('horario','Martes a domingo de 10:00 a.m. a 4:00 p.m.')}}</div>
            </p>
            <p class="text-gray-800 mt-4">
                <a href="mailto:{{setting('contact-email','contactopdlm@yucatan.gob.mx')}}">{{setting('contact-email','contactopdlm@yucatan.gob.mx')}}</a>
            </p>
        </div>
        <div class="flex-1 mt-8 lg:mt-0 w-full">
            @if(isset($message))
            <div class="bg-orange-500 text-orange-100 px-3 py-2 rounded my-4 text-center">
                {{$message}}
            </div>
            @endif
            <form action="{{route('tour')}}" method="POST">
                @csrf
                <div>
                    <div class="text-gray-700">Nombre de la Escuela</div>
                    <div>
                        <input required name="school" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Grado escolar en curso</div>
                    <div>
                        <input required name="grade" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Nombre del maestro responsable</div>
                    <div>
                        <input required name="fullname" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Teléfono de contacto</div>
                    <div>
                        <input required name="phone" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="tel">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Correo Electrónico</div>
                    <div>
                        <input required name="email" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="email">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Asunto</div>
                    <div>
                        <input required name="subject" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text">
                    </div>
                </div>

                <div class="mt-4">
                    <div class="text-gray-700">Mensaje</div>
                    <div><textarea name="message" class="h-24 border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="" id="" cols=" 30" rows="10"></textarea></div>
                </div>
                <div class="text-right mt-4 mb-8">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection