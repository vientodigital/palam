@component('mail::message')
# Escuela
{{$school}}

**Grado**

{{$grade}}

**Email**

{{$email}}

**Phone**

{{$phone}}

# Maestro
{{$fullname}}


# Mensaje
**{{$subject}}**

{{$message}}


Gracias,
{{ config('app.name') }}
@endcomponent