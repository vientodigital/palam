<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];
    protected $dates = ['starts_on', 'expires_on'];

    public function space()
    {
        return $this->belongsTo(Space::class);
    }

    public function scopeActive($query)
    {
        return $query->where('starts_on', '<=', now())->where('expires_on', '>', now());
    }
}
