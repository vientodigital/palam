<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    protected $guarded = [];

    public function scopeVisible($query)
    {
        return $query->where('visible', '=', 1);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
