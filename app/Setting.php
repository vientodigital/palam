<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Setting extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public static function text(string $key, $defaultValue = '')
    {
        return trim(strip_tags(self::firstOrCreate(['key' => $key], ['value' => $defaultValue, 'name' => Str::title($key)])->value));
    }

    public static function html(string $key, $defaultValue = '')
    {
        return self::firstOrCreate(['key' => $key], ['value' => $defaultValue, 'name' => Str::title($key)])->value;
    }
}
