<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function reports($year, $month)
    {
        $period_id = Period::where('year', $year)->where('month', $month)->first()->id;
        return $this->hasMany(Report::class)->where('period_id', $period_id);
    }
}
