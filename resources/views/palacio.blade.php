@extends('layouts.app')

@section('header')
<header class="h-100 bg-cover bg-no-repeat bg-center" style="background-image: url({{url('storage/images/slidePalacioMusica-03.jpg')}})">
</header>
@endsection

@section('content')
<div class="flex flex-col lg:flex-row container mx-auto ">
    <section class="lg:w-1/2 px-6">
        <div class="pt-8">
            <h2 class="text-2xl text-gray-900 uppercase">Palacio de la Música</h2>
            <h3 class="text-2xl text-palam-500 font-bold uppercase ">Centro nacional de la música mexicana </h3>
            <p class="mt-4 antialiased">
                El diseño arquitectónico del edificio que alberga el Palacio de la Música fue resultado de los trabajos
                realizados por Alejandro Medina Arquitectura, Reyes Ríos + Larraín Arquitectos, Muñoz Arquitectos y
                Quesnel
                Arquitectos y un amplio equipo de especialistas en el diseño y construcción de obra pública.
            </p>
            <p class="mt-4 antialiased">
                Es un edificio moderno que se inserta de manera sensible en el centro histórico de la ciudad de Mérida,
                Yucatán. Y destaca por dos criterios arquitectónicos de gran valor: el respeto a su entorno y su
                vocación social. <br>
                Las características del inmueble del Palacio de la Música muestran el interés fundamental de contribuir
                a la revitalización del espacio urbano, a través de la permeabilidad visual y conectividad peatonal, y
                fortalecer el sentido de pertenencia de la ciudadanía, tanto con el Palacio de la Música, como con el
                centro
                histórico de Mérida, Yucatán.
            </p>
        </div>
        <div class="pt-8">
            <h3 class="text-2xl text-palam-500 font-bold uppercase">Vision</h3>
            <p class="mt-4 antialiased">
                Ser el principal referente nacional e internacional acerca de la música tradicional y popular mexicana.
            </p>
        </div>
        <div class="pt-8">
            <h3 class="text-2xl text-palam-500 font-bold uppercase">Misión</h3>
            <p class="mt-4 antialiased">
                Presentar el panorama general de la música tradicional y popular mexicana para poner en valor, conservar
                y difundir, entre el público nacional y extranjero, su importancia como patrimonio cultural inmaterial;
                a través de una propuesta atractiva y amena que propicie el conocimiento, la emotividad y el
                entretenimiento de los más diversos visitantes.
            </p>
        </div>
    </section>
    <div class="lg:w-1/2 lg:flex lg:flex-col-reverse">
        <div class="flex flex-row lg:flex-col items-end lg:items-start lg:ml-6">
            <div class="mt-4">
                <img class="" src="{{asset('storage/images/ninaInteractivo-Palacio-Musica.png')}}" alt="">
            </div>
            <div class="mt-4">
                <img class="" src="{{asset('storage/images/salaConciertos-Palacio-Musica.png')}}" alt="">
            </div>
        </div>
        <section class="container mx-auto px-6 lg:mt-10">
            <div class="pt-8">
                <h3 class="text-2xl text-palam-500 font-bold uppercase">Espacios y servicios </h3>
                <p class="mt-4 antialiased">
                    El Palacio de la Música enfoca su labor en la preservación y divulgación de la música tradicional y
                    popular mexicana, por lo cual ofrece diferentes espacios y servicios dedicados al conocimiento y
                    disfrute de nuestro maravilloso patrimonio musical.
                </p>
                <ul class="mt-4 pl-4">
                    <li> Museo </li>
                    <li> Sala de conciertos </li>
                    <li> Acceso a la Red Nacional de Fonotecas </li>
                    <li> Eventos culturales </li>
                </ul>
            </div>
            <div class="pt-8">
                <h3 class="text-2xl text-palam-500 font-bold uppercase">SALA DE CONCIERTOS</h3>
                <p class="mt-4 antialiased">
                    La sala de conciertos tiene capacidad para 458 personas y cuenta con equipos de alta tecnología que
                    hacen posible llevar a cabo presentaciones musicales de primer nivel, así como charlas,
                    conferencias,
                    seminarios y actividades de distinta índole. Además de tener la posibilidad de funcionar como
                    estudio de
                    grabación.
                </p>
            </div>
            <div class="pt-8">
                <h3 class="text-2xl text-palam-500 font-bold uppercase">RED NACIONAL DE FONOTECAS </h3>
                <p class="mt-4 antialiased">
                    La Red Nacional de Fonotecas tiene una plataforma tecnológica que permite acceder al sistema de
                    consulta
                    del patrimonio sonoro digitalizado de la Fonoteca Nacional.
                </p>
            </div>
        </section>
    </div>
</div>
<div class="mt-8">
    <img class="w-full h-auto" src="{{asset('storage/images/Vista-Palacio-Musica.png')}}" alt="">
</div>
@endsection