@extends('layouts.app')
@section('header')

<header class="h-100  bg-cover bg-no-repeat bg-bottom" style="background-image: url({{url('storage/images/slidePalacioMusica-01.jpg')}})">
    <div class="absolute flex h-56 md:h-auto flex-col w-full sm:w-2/3 md:w-1/2 lg:w-2/5 h-48 p-4 mt-40 ml-0 xs:ml-8 md:ml-32 lg:ml-48 xl:ml-64 bg-palam-nav">
        <h2 class="font-bold text-white uppercase">La Música Mexicana</h2>
        <p class="text-white mt-2">
        La música en México es rica en variedad de géneros ritmos y temas.
        Es fruto del mestizaje entre tradiciones y tiene profundas raíces de
        lo prehispánico.
        </p>
        <!-- <p class="text-white mt-2">
            La música en México es rica en variedad de géneros, ritmos y temas. Es fruto del mestizaje entre las
            tradiciones europea y americana, pero tiene profundas raíces de lo prehispánico y de nuestra tercera raíz,
            que aunque poco reconocida en otros ámbitos culturales es en la música donde adquiere mayor relevancia.
        </p> -->
    </div>
</header>

@endsection

@section('content')


<div class="container mx-auto">
    <div class="py-8">
        <div class="flex flex-col md:flex-row justify-center">
            <div class="flex-1 mx-4">
                <a href="{{route('events')}}">
                    <img class="mb-2 h-64 object-cover" src="{{asset('storage/images/EVENTOS.JPG')}}" alt="Eventos">
                </a>
                <div>
                    <h2 class="my-4 text-lg font-semibold text-palam-500 uppercase">
                        <a href="{{route('events')}}"> Eventos </a>
                    </h2>
                    <p class="antialiased">Consulta los próximos conciertos, talleres y actividades.</p>
                </div>
            </div>
            <div class="flex-1 mx-4">
                <a href="{{route('museum')}}">
                    <img class="mb-2 h-64 object-cover" src="{{asset('storage/images/MUSEO INTERACTIVO.JPG')}}" alt="Museo Interactivo">
                </a>
                <div>
                    <h2 class="my-4 text-lg font-semibold text-palam-500 uppercase">
                        <a href="{{route('museum')}}">
                            Museo Interactivo
                        </a>
                    </h2>
                    <p class="antialiased">Realiza un recorrido por la historia de la música tradicional y popular mexicana.</p>
                </div>
            </div>
            <div class="flex-1 mx-4">
                <a href="{{route('spaces')}}">
                    <img class="mb-2 h-64 object-cover" src="{{asset('storage/images/ESPACIOS Y SERVICIOS.jpg')}}" alt="Espacios y Servicios">
                </a>
                <div class="">
                    <h2 class="my-4 text-lg font-semibold text-palam-500 uppercase">
                        <a href="{{route('spaces')}}"> Espacios y Servicios</a> </h2>

                    <p class="antialiased">Descubre los distintos espacios y servicios dedicados al conocimiento y disfruta del patrimonio musical de México.</p>
                </div>
            </div>
        </div>
        <div class="mt-10 bg-palam-100 flex flex-col flex-col-reverse md:flex-row md">
            <div class="flex-1 px-20 py-12">
                <h4 class="text-lg font-semibold text-palam-500 uppercase ">Recorridos Escolares Gratuitos</h4>
                <p class="mt-8">Dirigidos a grupos educativos integrados por estudiantes escolares o universitarios y sus profesores.</p>
                <a class="mt-8 inline-block btn btn-primary" href="{{route('tour')}}">Solicita recorrido</a>
            </div>
            <div class="flex-1">
                <img class="" src="{{asset('storage/images/IGH-96.png')}}" alt="">
            </div>
        </div>
        <div class="flex flex-col md:flex-row mt-8">
            <div class="flex-1">
                <img class="" src="{{asset('storage/images/TARIFAS.JPG')}}" alt="">
            </div>
            <div class="flex-1 ml-6 text-gray-800">
                <h5 class="text-palam-500 font-semibold text-lg uppercase mt-4 md:mt-0"> TARIFAS </h5>
                <p class="uppercase font-semibold">Museo Interactivo</p>
                <table class="mt-4">
                    <tbody>
                        <tr>
                            <td> Entrada General</td>
                            <td class="pl-8 text-right"> $150.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Nacionales</td>
                            <td class="text-right"> $100.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Residentes de Yucatán</td>
                            <td class="text-right"> $50.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Menores 0-3 años</td>
                            <td class="text-right"> $0.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Menores 4-12 años</td>
                            <td class="text-right"> $25.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Menores 13-17 años</td>
                            <td class="text-right"> $50.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Maestros y estudiantes</td>
                            <td class="text-right"> $25.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Adultos mayores (INAPAM)</td>
                            <td class="text-right"> $25.00 MXN </td>
                        </tr>
                        <tr>
                            <td> Personas con discapacidad</td>
                            <td class="text-right"> $25.00 MXN </td>
                        </tr>
                    </tbody>
                </table>
                <p class="mt-4">* Acreditar a través de documento oficial vigente (INE, licencia de conducir, pasaporte,
                    INAPAM, credencial de maestro o estudiante) presentado físicamente.</p>

                <h5 class="mt-4 text-gray-600 font-semibold"> Martes </h5>
                <p class="mt-2">
                    Entrada libre para residentes de Yucatán al presentar físicamente documento oficial
                    vigente (INE, licencia de conducir o pasaporte). Los menores de edad que acompañen
                    a los acreditados entrarán gratis.
                </p>

                <h5 class="mt-4 text-gray-600 font-semibold">Horario</h5>
                <div class="whitespace-pre-line">{{setting('horario','Martes a domingo de 10:00 a.m. a 4:00 p.m.')}}</div>
            </div>

        </div>
    </div>
</div>

@endsection