@extends('layouts.app')

@section('header')
<header class="h-100 md:h-140 lg:h-180 bg-cover bg-no-repeat bg-center" style="background-image: url({{url('storage/images/slidePalacioTrans.jpg')}})">
    <div class="container mx-auto flex flex-col lg:flex-row h-full items-center lg:justify-around justify-center">
        <div class="mt-4">
            <a class="uppercase text-white" target="_blank" href="https://www.plataformadetransparencia.org.mx/web/guest/inicio">
                Plataforma nacional de transparencia
            </a>

        </div>
        <div class="mt-4">
            <a class="uppercase text-white" href="{{route('accounting')}}">
                Contabilidad gubernamental
            </a>
        </div>
        <div class="mt-4">
            <a class="uppercase text-white" target="_blank" href="https://consultapublicamx.inai.org.mx/vut-web/faces/view/consultaPublica.xhtml#sujetosObligados">
                Plataforma estatal de transparencia
            </a>
        </div>
    </div>
</header>
@endsection

@section('content')
@endsection