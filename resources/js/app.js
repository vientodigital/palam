window.opened = false;
function toggleNav() {
    var width = window.opened ? '0' : '14rem';
    document.getElementById("sidenav").style.width = width;
    document.getElementById("app").style.marginLeft = width;
    // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    window.opened = !window.opened;
}
window.toggleNav = toggleNav;
function closePopup() {
    console.log('close');
    document.getElementById('popup').classList.add('hidden');
}
window.closePopup = closePopup
function openPopup(obj) {
    var evt = JSON.parse(obj);
    document.getElementById('popup').classList.remove('hidden');
    document.getElementById('popup-title').innerHTML = evt.title;
    document.getElementById('popup-content').innerHTML = evt.content;
    document.getElementById('popup-photo').src = 'storage/'+evt.photo_url;

}
window.openPopup = openPopup