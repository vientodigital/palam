<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('to/{locale}', function ($locale) {
    App::setLocale($locale);
});

Route::get('/', 'PageController@home')->name('home');
Route::get('/espacios', 'PageController@spaces')->name('spaces');
Route::get('/fonoteca', 'PageController@soundLibrary')->name('sound-library');
Route::get('/eventos', 'PageController@events')->name('events');
Route::get('/museo', 'PageController@museum')->name('museum');
Route::get('/palacio', 'PageController@palacio')->name('palacio');
Route::get('/transparencia', 'PageController@trans')->name('trans');
Route::get('/contacto', 'PageController@contact')->name('contact');
Route::post('/contacto', 'PageController@contactPost')->name('contact.post');
Route::get('/tour', 'PageController@tour')->name('tour');
Route::post('/tour', 'PageController@tourPost')->name('tour.post');
// Route::get('/directorio', 'PageController@directory')->name('directory');
Route::get('/contabilidad/{year?}/{month?}', 'PageController@accounting')->name('accounting');

// Route::get('mailable', function () {
//     $data = [
//         'fullname' => 'Lorem, ipsum dolor.',
//         'email' => 'email@foo.com',
//         'subject' => 'Subject: Lorem, ipsum.',
//         'message' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Expedita, placeat et quibusdam laudantium assumenda aut!',
//     ];

//     return new App\Mail\ContactForm($data);
// });

Route::get('/{section}/{page?}', 'PageController@post')->name('post')->where('section', '^(?!nova.*$)[0-9a-z\-]+');
