<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
