@extends('layouts.app')

@section('tail')
<script>
        var dropdowns = document.getElementsByClassName('dropdown');

        for (let i = 0; i < dropdowns.length; i++) {
                dropdowns[i].addEventListener("click", function () {
                        console.log("Clicked index: " + i);
                        if (this.parentNode.classList.contains('show')) {
                                this.parentNode.classList.remove('show');
                        } else {
                                this.parentNode.classList.add('show');
                        }
                })
        }

</script>
@endsection



@section('header')
<header class="h-100 bg-cover bg-no-repeat bg-center" style="background-image: url({{url('storage/images/slidePalacioMusica-01.jpg')}})"></header>
@endsection
@section('content')
<section class="container mx-auto pt-6 pb-8">
        <h2 class=" mx-6 text-2xl text-palam-500 uppercase"> <a href="{{route('accounting',['year'=>$year])}}">Contabilidad Gubernamental </a> </h2>
        <div class="mx-6 my-6 flex antialiased">
                <div class="flex flex-col w-32 ">
                        <div class="dropdown flex items-center justify-between text-lg font-semibold bg-palam-500 text-white py-1 px-2 cursor-pointer">
                                Año
                                @svg('chevron-down','fill-current text-white h-3 mr-2',[])
                        </div>
                        <div class="item absolute w-32 mt-8 shadow-lg">
                                @foreach($years as $y)
                                <div class="bg-palam-700 text-white pt-1 px-2 "> <a href="{{route('accounting',['year'=>$y])}}" class="block">{{$y}}</a> </div>
                                @endforeach
                        </div>
                </div>
                <div class="flex flex-col w-32 ml-6">
                        <div class="dropdown flex items-center justify-between text-lg font-semibold bg-palam-500 text-white py-1 px-2 cursor-pointer">

                                Mes
                                @svg('chevron-down','fill-current text-white h-3 mr-2',[])
                        </div>
                        <div class="item absolute w-32 mt-8 shadow-lg">
                                @foreach($periods as $p)
                                <div class="bg-palam-700 text-white  pt-1 px-2 ">
                                        <a href="{{route('accounting',['year'=>$p->year,'month'=>$p->month])}}" class="block capitalize">@if($p->trimester){{$p->trimester_name}}@else{{$p->month_name}}@endif</a>
                                </div>
                                @endforeach
                        </div>
                </div>
        </div>
        @if(is_null($month))

        <ol class="flex flex-col lg:flex-row lg:flex-wrap justify-around">
                @foreach($periods as $p)
                <li class="lg:w-5/12 bg-palam-100 mx-4 mt-4 antialiased">
                        <a href="{{route('accounting',['year'=>$p->year,'month'=>$p->month])}}" class="py-4 px-6 text-white block capitalize">{{$p->month_name}}</a>
                </li>
                @endforeach
        </ol>
        @else
        <h4 class="mx-6 my-4 font-semibold text-2xl text-palam-500 uppercase">{{$period->month_name}} {{$year}}</h4>
        <div class="flex flex-col justify-around lg:flex-row items-start mx-6">
                @foreach($categories as $c)
                <div class="mb-4">
                        <div class="text-palam-500 font-bold uppercase  ">{{$c->name}}</div>
                        <ol class="flex flex-col">
                                @forelse($c->reports($year,$month)->get() as $r)
                                <li class="ml-4 py-1">
                                        <a class="hover:text-palam-100" href="{{url("storage/{$r->filename}")}}" download>{{$r->name}}</a>
                                </li>
                                @empty
                                <li class="ml-4 py-1 text-gray-500">
                                        No hay reportes disponibles
                                </li>
                                @endforelse
                        </ol>
                </div>
                @endforeach
        </div>
        @endif
</section>


@endsection