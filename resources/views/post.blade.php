@extends('layouts.app')

@section('header')
@if($post)
<div class="mt-32"></div>
@else
<header class="h-80 bg-cover bg-no-repeat bg-center"
    style="background-image: url({{url('storage/images/press-mic.jpg')}})">
</header>
@endunless
@endsection

@section('content')

<section class="container mx-auto pt-6 pb-8 px-6">
    <h4 class="text-2xl uppercase"> Palacio de la Música </h4>

    @if($post)
    <article class="">
        <h2 class="text-2xl uppercase text-palam-500 font-bold mt-4"> <a
                href="{{url($section->slug)}}">{{$section->name}}
            </a></h2>
        <div class="flex flex-col lg:flex-row items-center">
            <div class="mt-4">
                <h1 class="text-2xl uppercase text-palam-500 font-bold">{{$post->title}}</h1>
                <h2 class="text-gray-900 text-sm">{{$post->description}}</h2>
            </div>
            <div class="mt-4 lg:ml-4">
                <img class="w-full" src="{{url("storage/{$post->filename}")}}" alt="{{$post->title}}">
                <div class="mt-6 text-gray-800 antialiased">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
    </article>

    @else

    <h1 class="text-2xl uppercase text-palam-500 font-bold"> {{$section->name}} </h1>
    <div class="flex flex-col lg:flex-row lg:flex-wrap justify-between">
        @forelse($section->posts()->latest()->get() as $p)
        <div class="lg:w-5/12">
            <div class="py-8">
                <a href="{{route('post',['section'=>$section->slug,'post'=>$p->slug])}}">
                    <img class="w-full" src="{{url($p->filename)}}" alt="{{$p->title}}">
                </a>
            </div>
            <h2 class="text-palam-500 uppercase font-bold">
                <a href="{{route('post',['section'=>$section->slug,'post'=>$p->slug])}}">
                    {{$p->title}}
                </a>
            </h2>
            <h3> {{$p->description}} </h3>
        </div>
        @empty
        <div>
            Proximamente
        </div>
        @endforelse
    </div>
    @endif

</section>
@endsection
