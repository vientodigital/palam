<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TourForm extends Mailable
{
    use Queueable, SerializesModels;

    public $school;
    public $grade;
    public $email;
    public $fullname;
    public $phone;
    public $subject;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->school = $data['school'];
        $this->grade = $data['grade'];
        $this->email = $data['email'];
        $this->fullname = $data['fullname'];
        $this->phone = $data['phone'];
        $this->subject = $data['subject'];
        $this->message = $data['message'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.tour-form');
    }
}
