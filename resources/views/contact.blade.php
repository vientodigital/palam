@extends('layouts.app')

@section('header')
<header class="bg-bottom bg-no-repeat bg-cover h-100" style="background-image: url({{url('storage/images/slidePalacioMusica.jpg')}})">
</header>
<head>
   {!! NoCaptcha::renderJs() !!}
</head>
@endsection

@section('content')
<section class="container mx-auto">
    <div class="flex flex-col items-start justify-between px-6 pt-8 lg:flex-row">
        <div class="flex-1">
            <h1 class="text-2xl uppercase text-palam-500">Contáctanos</h1>
            <p class="mt-4 text-gray-800">
                {!!setting('direccion','Calle 58 núm. 497 x 59 y 60, <br/>Centro Histórico, <br/>Mérida, Yucatán.',true)!!}
            </p>
            <p class="mt-4 text-gray-800">
                <h4>Horarios</h4>
                <div class="whitespace-pre-line"> {{setting('horario','Martes a domingo de 10:00 a.m. a 4:00 p.m.')}} </div>
            </p>
            <p class="mt-4 text-gray-800">
                <a href="mailto:{{setting('contact-email','contactopdlm@yucatan.gob.mx')}}">{{setting('contact-email','contactopdlm@yucatan.gob.mx')}}</a>
            </p>
            <p class="mt-4">
                <a href="http://www.yucatan.gob.mx/gobierno/detalle.php?id_d=87" class="btn btn-primary">Directorio</a>
            </p>
        </div>
        <div class="flex-1 w-full mt-8 lg:mt-0">
            @if(isset($message))
            <div class="px-3 py-2 my-4 text-center text-orange-100 bg-orange-500 rounded">
                {{$message}}
            </div>
            @endif
            <form action="{{route('contact')}}" method="POST">
                @csrf
                <div>
                    <div class="text-gray-700">Nombre</div>
                    <div>
                        <input name="fullname" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500
                            type=" text">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Correo Electrónico</div>
                    <div>
                        <input name="email" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500
                            type=" email">
                    </div>
                </div>
                <div>
                    <div class="text-gray-700">Asunto</div>
                    <div>
                        <input name="subject" class="border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500
                            type=" text">
                    </div>
                </div>

                <div class="mt-4">
                    <div class="text-gray-700">Mensaje</div>
                    <div><textarea name="message" class="h-24 border border-2 border-palam-500 appearance-none block w-full bg-gray-100 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500
                            name="" id="" cols=" 30" rows="10"></textarea></div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        {!! NoCaptcha::display() !!}
                    </div>
                </div>
                @if ($errors->has('g-recaptcha-response'))
                    <span class="text-red-700 help-block" role="alert">
                        <strong>Completar recaptcha</strong>
                    </span>
                @endif
                <div class="mt-4 mb-8 text-right">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="border-t-4 border-palam-500">
    <iframe class="w-full h-64" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14902.40810700236!2d-89.6215745!3d20.9684914!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfe685b8e8345c314!2sPalacio%20de%20la%20M%C3%BAsica!5e0!3m2!1sen!2smx!4v1581096454384!5m2!1sen!2smx" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
@endsection