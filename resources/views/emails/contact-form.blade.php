@component('mail::message')
# Contacto
{{$fullname}}
{{$email}}

# Asunto
{{$subject}}

# Mensaje
{{$message}}

Gracias,<br>
{{ config('app.name') }}
@endcomponent