<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Period;
use App\Category;
use App\Section;
use App\Post;
use App\Mail\ContactForm;
use App\Mail\TourForm;
use App\Space;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function spaces()
    {
        $spaces = Space::whereVisible(true)->orderBy('position')->get();
        return view('spaces', compact(['spaces']));
    }

    public function events()
    {
        $spaces = Space::whereVisible(true)->orderBy('position')->get();
        return view('events', compact(['spaces']));
    }

    public function museum()
    {
        $space = Space::whereVisible(false)->whereName('Museo Interactivo')->with('events')->first();
        return view('museum', compact(['space']));
    }

    public function palacio()
    {
        return view('palacio');
    }

    public function soundLibrary()
    {
        return view('sound-library');
    }

    private function years()
    {
        $years = DB::table('periods')->groupBy('year')->select('year')->pluck('year')->toArray();
        return $years;
    }

    public function accounting(Request $request, int $year = null, int $month = null)
    {
        $years = $this->years();
        $year = $year ?? Carbon::now()->year;
        $categories = Category::orderBy('name')->get();
        $periods = Period::where('year', $year)->orderBy('month')->orderBy('trimester')->get();

        if (!is_null($month)) {
            $period = Period::where('year', $year)->where('month', $month)->first();
        } else {
            $period = Period::where('year', $year)->orderBy('month')->orderBy('trimester')->first();
            $month = $period->month;
        }
        $period_ids = $periods->pluck('id');
        $reports = Report::whereIn('period_id', $period_ids)->get();
        return view('accounting', compact(['years', 'year', 'month', 'categories', 'periods', 'period', 'reports']));
    }

    public function post(Request $request, string $section_slug, string $post_slug = null)
    {
        $section = Section::whereSlug($section_slug)->first();
        if (is_null($section)) {
            abort(404);
        }
        $post = null;
        if (!is_null($post_slug)) {
            $post = Post::whereSlug($post_slug)->first();
        }
        return view('post', compact(['section', 'post']));
    }

    public function trans()
    {
        return view('trans');
    }

    public function contact()
    {
        return view('contact');
    }

    public function contactPost(Request $request)
    {
        $request->validate([
            'g-recaptcha-response' => 'required|captcha',
        ]);
        $data = $request->all();
        Mail::to(env('MAIL_TO_ADDRESS', 'contactopdlm@yucatan.gob.mx'))->send(new ContactForm($data));
        return view('contact', ['message' => 'Mensaje Enviado']);
    }

    public function tour()
    {
        return view('tour');
    }

    public function tourPost(Request $request)
    {
        $data = $request->all();
        Mail::to(env('MAIL_TO_ADDRESS', 'contactopdlm@yucatan.gob.mx'))->send(new TourForm($data));
        return view('tour', ['message' => 'Solicitud Escolar Enviada']);
    }
}
