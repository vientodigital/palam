<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Period extends Model
{
    protected $appends = ['name', 'month_name'];

    public function getNameAttribute()
    {
        return "{$this->year} / {$this->month}";
    }

    public function getMonthNameAttribute()
    {
        return Carbon::parse("{$this->year}-{$this->month}-1 ")->monthName;
    }

    public function getTrimesterNameAttribute()
    {
        return $this->getTrimesterName($this->month);
    }

    private function getTrimesterName($month)
    {
        if ($month >= 1 && $month < 4) {
            return '1er Trimestre';
        } elseif ($month >= 4 && $month < 7) {
            return '2o Trimestre';
        } elseif ($month >= 7 && $month < 10) {
            return '3er Trimestre';
        } elseif ($month >= 10 && $month <= 12) {
            return '4o Trimestre';
        }
        return '';
    }
}
