@extends('layouts.app')

@section('header')
<header class="h-80 bg-cover bg-no-repeat bg-center" style="background-image: url({{url('storage/images/slidePalacioMusica-01.jpg')}})"> </header>
@endsection

@section('content')

<div class="flex flex-col lg:flex-row container mx-auto h-screen">
    <section class="px-6 my-4">
        <h4 class="mt-4 text-2xl text-gray-900 uppercase"> FONOTECA PRÓXIMAMENTE </h4>

        <p class="mt-4">
            La fonoteca del Palacio de la Música es un espacio físico donde se encuentran terminales de cómputo conectadas al sistema de consulta de audios de la Fonoteca Nacional y que tiene como finalidad acercar al público a una parte fundamental de la herencia cultural de la nación: el patrimonio sonoro, y a la vez busca fomentar la cultura de la escucha en la comunidad donde se instala.
        </p>

    </section>
</div>

@endsection