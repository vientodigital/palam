@extends('layouts.app')

@section('header')
<header class="h-80 bg-cover bg-no-repeat bg-center" style="background-image: url({{url('storage/images/slidePalacioMusica-01.jpg')}})"> </header>
@endsection

@section('content')

<div class="flex flex-col lg:flex-row container mx-auto ">
    <section class="px-6 my-4">
        <h4 class="mt-4 text-2xl text-gray-900 uppercase">PALACIO DE LA MÚSICA </h4>
        <h3 class="text-2xl text-palam-500 font-bold uppercase">CENTRO NACIONAL DE LA MÚSICA MEXICANA</h3>
        <p class="mt-4 text-gray-800 antialiased"> Es un edificio moderno que se inserta de manera sensible en el centro histórico de la ciudad de Mérida, Yucatán.
            Y destaca por dos criterios arquitectónicos de gran valor: el respeto a su entorno y su vocación social. </p>

        <p class="mt-4 text-gray-800 antialiased"> Las características del inmueble del Palacio de la Música muestran el interés fundamental de contribuir a la
            revitalización del espacio urbano, a través de la permeabilidad visual y conectividad peatonal, y fortalecer
            el sentido de pertenencia de la ciudadanía, tanto con el Palacio de la Música, como con el centro histórico de
            Mérida, Yucatán. </p>

        <h1 class="mt-8 text-2xl text-palam-500 font-bold uppercase">ESPACIOS Y SERVICIOS</h1>
        <div class="flex flex-col md:flex-row flex-wrap items-start justify-between mt-6">
            @forelse($spaces as $space)
            <div class="w-full lg:w-1/2 my-4 px-6">
                <img class="h-80 w-full object-cover" src="{{asset('storage/'.$space->photo_url)}}" alt="">
                <h2 class="mt-4 capitalize text-lg font-bold text-gray-800 antialiased">{{$space->name}}</h2>
                <p class="mt-2 text-gray-700 antialiased">{{$space->description}}</p>
            </div>

            @empty
            No hay espacios definidos
            @endforelse
        </div>
        <div class="my-6 text-sm text-gray-800 antialiased">
            <span class="uppercase font-semibold">Mas Información</span> <br>
            <div class="whitespace-pre-line">
                {!!setting('mas-informacion','Coordinación de ventas <br> Teléfono: 923 0641 ext 1004 <br> Correo: <a href="mailto:palaciomusica@yucatan.gob.mx">palaciomusica@yucatan.gob.mx</a>','true')!!}
            </div>
        </div>
    </section>
</div>

@endsection