@extends('layouts.app')

@section('header')
<header class="h-100 bg-cover bg-no-repeat bg-center" style="background-image: url({{url('storage/images/slidePalacioMusica-01.jpg')}})"></header>
@endsection

@section('content')
<div class="absolute mt-20 z-10 w-full">
    <div id="popup" class="popup-fixed hidden bg-palam-500 text-gray-100  shadow-lg">
        <div class="float-right text-4xl w-8 h-8 mr-2 cursor-pointer hover:text-gray-400" onclick="closePopup('popup')">&times;</div>
        <div class="py-8">
            <div class="flex justify-between">
                <h2 id="popup-title" class="mx-16 mb-4 text-2xl tracking-wide uppercase font-semibold">Lorem ipsum dolor sit amet.</h2>
            </div>
            <div class="flex flex-col lg:flex-row justify-between mx-16 ">
                <div class="w-full pb-2 lg:w-1/3 lg:pb-0">
                    <img id="popup-photo" class="w-full lg:h-64 lg:w-64 object-cover" src='http://unsplash.it/300/300?random' alt='' />
                </div>
                <div id="popup-content" class="pl-4 w-2/3">
                    <p class="whitespace-pre-line">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.

                        Obcaecati, ipsam explicabo molestiae tempora ducimus unde? Dolor adipisci esse possimus, nobis ab tenetur commodi ullam rem
                        saepe maxime impedit autem quo laboriosam cumque odit. Rem, veniam.
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container mx-auto pt-6 pb-8 px-6">
    <div class="py-8">
        <h1 class="font-bold text-palam-500 text-xl uppercase">Eventos</h1>
        @forelse($spaces as $space)
        <div class="my-8">
            <h2 class="text-lg font-semibold text-palam-500">{{$space->name}}</h2>
            <div class="flex mt-4">
                @forelse($space->events()->active()->orderBy('expires_on','desc')->get() as $event)
                <div class="ml-4 first:ml-0">
                    <a href='javascript:openPopup(JSON.stringify({!!$event!!}))'>
                    <img class="object-cover w-64 h-64" src="{{asset('storage/'.$event->photo_url)}}" alt="{{$event->title}}"></a>
                    <p class="mt-4">
                        <a href='javascript:openPopup(JSON.stringify({!!$event!!}))'>{{$event->title}}</a>
                    </p>
                </div>
                @empty
                No hay eventos proximos
                @endforelse
            </div>
            @empty
            No hay eventos por el momento.
        </div>
        @endforelse
    </div>

</div>
@endsection