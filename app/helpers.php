<?php

use App\Setting;

if (!function_exists('setting')) {
    function setting($key, $default = '', $html = false)
    {
        if ($html) {
            return Setting::html($key, $default);
        }
        return Setting::text($key, $default);
    }
}
