module.exports = {
  theme: {
    fontfamily: {
      sans: [
        'Panton',
        '-apple-system',
        'blinkmacsystemfont',
        '"segoe ui"',
        'roboto',
        '"helvetica neue"',
        'arial',
        '"noto sans"',
        'sans-serif',
        '"apple color emoji"',
        '"segoe ui emoji"',
        '"segoe ui symbol"',
        '"noto color emoji"',
      ],
      serif: [
        'georgia',
        'cambria',
        '"times new roman"',
        'times',
        'serif',
      ],
      mono: [
        'menlo',
        'monaco',
        'consolas',
        '"liberation mono"',
        '"courier new"',
        'monospace',
      ],
    },
    extend: {
      colors: {
        'palam': {
          100: '#EBEEF6',
          200: '#C8CBD7',
          300: '#A7ACBE',
          400: '#666D8E',
          500: '#242F5D',
          600: '#202A54',
          700: '#161C38',
          800: '#10152A',
          900: '#0B0E1C',
          'nav': 'rgba(36, 47, 93,.9)'
        },
      },
      height: {
        72: '18rem',
        80: '20rem',
        100: '25rem',
        120: '30rem',
        140: '35rem',
        180: '45rem',
        200: '50rem',
      },
      width: {
        80: '20rem',
        100: '25rem',
        120: '30rem',
        140: '35rem',
      },
      minWidth: {
        'screen': '100vw',
      }
    },

  },
  variants: {
    margin: ['responsive', 'first', 'last']
  },
  plugins: []
}
